import '@/styles/globals.css'
import 'aos/dist/aos.css'; // Import the AOS styles
import AOS from 'aos'; 
import { useEffect } from 'react';

export default function App({ Component, pageProps }) {
  useEffect(() => {
    // Initialize AOS on component mount
    AOS.init({
      duration: 1000, // Animation duration
      once: true, // Whether animation should happen only once
    });

    // Clean up AOS on component unmount
    return () => {
      AOS.refresh();
    };
  }, []); 
  return <Component {...pageProps} />
}
